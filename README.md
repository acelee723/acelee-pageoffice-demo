# acelee-pageoffice-demo

#### 介绍
spring boot/spring cloud集成卓正PageOffice的demo，能够实现在线编辑、打开word、excel等office。

#### 软件架构

spring boot web

spring boot thymeleaf

pageoffice4.5.0.6


#### 安装教程


启动项目，访问http://localhost:8080/index，点击打开文件，会提示安装卓正的Page Office，

点击安装，成功后会提示注册，这时候把第一步下载的序列号输入，其他的随便填写，就可以打开word的编辑了；